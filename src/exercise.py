import numpy as np
import pandas as pd
import os

def read_data(path_):
    """
    Args: path_, the path to the csv file
    Returns: df, the dataframe
    """
    try:
        df = pd.read_csv(path_)
    except:
        print("some error occured")


def merge_df(df1, df2):
    """
    It merge 2 dataframes.
    Args: df1, df2: dataframes
    """
    df = pd.merge([df1,df2], axis=1)
    return df

def split_data(df, test_pct=0.7, val_pct=0.15, test_pct=0.15):
    """
    Split the dataframe into train, val, test
    Args:
        df: The dataframe which will be splitted
    Returns:
        train, val, test: 3 dataframes, default ratio 70, 15, 15
    """
    df = df.shuffle()
    train_size = int(df.shape[0]*0.75)
    val_size = int(df.shape[0]*0.15)
    test_size = int(df.shape[0]*0.15)

    train = df.iloc[0:train_size, :]
    val = df.iloc[train_size:val_size, :]
    test = df.iloc[val_size:, :]
    return train, test, val


def save_df(df, path="", filename="data"):
    """Save the dataframe in the given folder
    Args:
        df: dataframe
        path: path where df will be stored
    Returns:
        None
     """
    if os.path.exists(path):
         df.to_csv(filename+".csv", index=False)
    else:
        print("given path doesn't exist")